import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:login_register/constant/utils.dart';

Future userLogin(String email,String Password) async{
  final response=await http.post(Uri.parse('${Utils.baseUrl}/users/login'),
      headers: {
        'Content-Type': 'application/json',
      },
      body: jsonEncode({'usuario_email': email, 'usuario_password': Password}));



  final List<dynamic> decodedData=jsonDecode(response.body);
  return decodedData;

}


Future userRegister(String username,String email,String Password) async{
  final response=await http.post(Uri.parse('${Utils.baseUrl}/users/register'),
      headers: {"Accept":"Application/json"},
      body: {'usuario_nombre':username,'usuario_email':email,'usuario_password':Password}
  );


  var decodedData=jsonDecode(response.body);
  return decodedData;
}