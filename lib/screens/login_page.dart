import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:login_register/constant/utils.dart';
import 'package:login_register/mocks/mock_data.dart';
import 'package:login_register/models/usuario.dart';
import 'package:login_register/screens/home_page.dart';
//import 'package:login_register/screens/register_page.dart';
import 'package:login_register/widgets/form_fields_widgets.dart';
import 'package:login_register/rest/rest_api.dart';
import 'package:http/http.dart' as http;
import '../item.dart';



class LoginPage extends StatefulWidget
{
  @override
  State<StatefulWidget> createState() {
   return _LoginPageState();
  }

}

class _LoginPageState extends State<LoginPage>{
  final String serverUrl = Utils.baseUrl;
  final GlobalKey<FormState> _formKey= GlobalKey<FormState>();
  final TextEditingController _emailController=TextEditingController();
  final TextEditingController _passwordController=TextEditingController();
// call shared preference object here
  final nameController = TextEditingController();
  final theemailController = TextEditingController();
  final thepasswordController = TextEditingController();
  Future<Item> addItem(String usuario_nombre, String usuario_email, String usuario_password) async {
    final response = await http.post(Uri.parse('$serverUrl/users'),
        headers: {
          'Content-Type': 'application/json',
        },
        body: jsonEncode({'usuario_nombre': usuario_nombre, 'usuario_email': usuario_email, 'usuario_password': usuario_password}));

    if (response.statusCode == 201) {
      final dynamic json = jsonDecode(response.body);
      final Item item = Item.fromJson(json);
      return item;
    } else {
      throw Exception('Error agregando al usuario');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          decoration: BoxDecoration(
              gradient: new LinearGradient(
                  colors: [Colors.white,Colors.black12],
                  begin: const FractionalOffset(0.0, 1.0),
                  end: const FractionalOffset(0.0, 1.0),
                  stops: [0.0,1.0],
                  tileMode: TileMode.repeated
              )
            // same code from main.dart we copy paste here
          ),
          child: Column(
            mainAxisSize: MainAxisSize.max,
            children: [
              SizedBox(height: 100,),
              SizedBox(height: 10,),
              Text('Inicio de sesion', style: TextStyle(color: Colors.purple, fontWeight: FontWeight.bold, fontSize: 15),),
              SizedBox(height: 40,),
              Form(
                  key: _formKey,
                  child: Column(
                    children: [
                      FormFields(
                        controller: _emailController,
                        data: Icons.email,
                        txtHint: 'Email',
                        obsecure: false, // this means if field type is password then sed true for *****
                      ),
                      FormFields(
                        controller: _passwordController,
                        data: Icons.lock,
                        txtHint: 'Password',
                        obsecure: true, // this means if field type is password then sed true for *****
                      )
                    ],
                  )
              ),
              SizedBox(height: 15,),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text('Te olvidaste el password?',style: TextStyle(color: Colors.white,fontSize: 12),),
                  SizedBox(width: 15,),
                  ElevatedButton(onPressed: (){
                    _emailController.text.isNotEmpty && _passwordController.text.isNotEmpty
                        ? doLogin(_emailController.text,_passwordController.text)
                        : Fluttertoast.showToast(msg: 'Todos los campos son requeridos',textColor: Colors.red);

                  },
                    style: ElevatedButton.styleFrom(
                        backgroundColor: Colors.purple,
                        padding: EdgeInsets.symmetric(horizontal: 50, vertical: 20),
                        textStyle: TextStyle(
                            fontSize: 15,
                            fontWeight: FontWeight.bold)),
                    child: Text("Iniciar sesion",style: TextStyle(color: Colors.white),),
                  )
                ],
              ),
              SizedBox(height: 20,),
              // InkWell(
              //   onTap: (){
              //     Navigator.push(context, MaterialPageRoute(builder: (context)=>RegisterPage()));
              //   },
              //   child: Row(
              //     mainAxisAlignment: MainAxisAlignment.center,
              //     children: [
              //       Text('No tienes una cuenta',style: TextStyle(color: Colors.white),),
              //     ],
              //   ),
              // )

            ],
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          showDialog(
              context: context,
              builder: (context) {
                return AlertDialog(
                  title: const Text('Agregar usuario'),
                  content:SingleChildScrollView(
                    child:Container(
                      child:Column(
                        children: [
                          TextFormField(
                            controller: nameController,
                            decoration: const InputDecoration(
                              labelText: 'Nombre',
                            ),
                          ),
                          TextFormField(
                            controller: theemailController,
                            decoration: const InputDecoration(
                              labelText: 'Email',
                            ),
                          ),
                          TextFormField(
                            controller: thepasswordController,
                            decoration: const InputDecoration(
                              labelText: 'Password',
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),


                  actions: [
                    TextButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      child: const Text('Cancelar'),
                    ),
                    TextButton(
                      onPressed: () {
                        addItem(
                            nameController.text, theemailController.text, thepasswordController.text);
                        setState(() {
                          nameController.clear();
                          theemailController.clear();
                          thepasswordController.clear();
                        });
                        Navigator.pop(context);
                      },
                      child: const Text('Guardar'),
                    ),
                  ],
                );
              });
        },
        tooltip: 'Create una cuenta',
        child: const Icon(Icons.add),
      ),

    );
  }

  doLogin(String email, String password) async {
    final res=await userLogin(email.trim(), password.trim());
    print('lol');
    print(userLogin);
    print('lol');
    print(res.toString());
      //here we set data
      print(res.toString().substring(14,16).trim());
      var este=res.toString().substring(14,16).replaceAll(RegExp('}'), '');
      int userId=int.parse(este.trim());
      MockData.user=Usuario(id: userId.toString(), nombre: email, email: email,password: password);
      MockData.getAllProductos();
      Route route=MaterialPageRoute(builder: (_)=>PageStatefull(userId));
      Navigator.pushReplacement(context, route);



  }



}