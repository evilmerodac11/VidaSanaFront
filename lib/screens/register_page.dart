import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:login_register/item.dart';
import 'package:http/http.dart' as http;


void main() {
  runApp(const RegisterPage());
}

class RegisterPage extends StatelessWidget {
  const RegisterPage({super.key});


  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
      ),
      home: const Home(),
    );
  }
}

class Home extends StatefulWidget {
  const Home({super.key});

  @override
  State<Home> createState() => _HomeState();
}

class _HomeState extends State<Home> {
  final String serverUrl = 'http://192.168.137.247:5000';

  final nameController = TextEditingController();
  final emailController = TextEditingController();
  final passwordController = TextEditingController();


  Future<List<Item>> fetchItems() async {
    final response = await http.get(Uri.parse('$serverUrl/users'));

    if (response.statusCode == 201) {
      final List<dynamic> itemList = jsonDecode(response.body);
      final List<Item> items = itemList.map((item) {
        return Item.fromJson(item);
      }).toList();
      return items;
    } else {
      throw Exception("Error recuperando los datos");
    }
  }








  Future<Item> addItem(String usuario_nombre, String usuario_email, String usuario_password) async {
    final response = await http.post(Uri.parse('$serverUrl/users'),
        headers: {
          'Content-Type': 'application/json',
        },
        body: jsonEncode({'usuario_nombre': usuario_nombre, 'usuario_email': usuario_email, 'usuario_password': usuario_password}));

    if (response.statusCode == 201) {
      final dynamic json = jsonDecode(response.body);
      final Item item = Item.fromJson(json);
      return item;
    } else {
      throw Exception('Error agregando al usuario'+response.statusCode.toString());
    }
  }


  Future<Item> addItemproducts(String producto_nombre, double producto_calorias, double producto_proteinas, double producto_grasas, int usuario_id) async {
    final response = await http.post(Uri.parse('$serverUrl/api/products'),
        headers: {
          'Content-Type': 'application/json',
        },
        body: jsonEncode({'producto_nombre': producto_nombre, 'producto_calorias': producto_calorias, 'producto_proteinas': producto_proteinas, 'producto_grasas': producto_grasas, 'usuario_id': usuario_id}));

    if (response.statusCode == 201) {
      final dynamic json = jsonDecode(response.body);
      final Item item = Item.fromJson(json);
      return item;
    } else {
      throw Exception('Error agregando al producto');
    }
  }




  Future<Item> addItemfoods(String alimentacion_fecha, int producto_id, double alimentacion_cantidad, int usuario_id) async {
    final response = await http.post(Uri.parse('$serverUrl/api/food/ali'),
        headers: {
          'Content-Type': 'application/json',
        },
        body: jsonEncode({'alimentacion_fecha': alimentacion_fecha, 'producto_id': producto_id, 'alimentacion_cantidad': alimentacion_cantidad, 'usuario_id': usuario_id}));

    if (response.statusCode == 201) {
      final dynamic json = jsonDecode(response.body);
      final Item itemf = Item.fromJson(json);
      return itemf;
    } else {
      throw Exception('Error agregando la alimentacion');
    }
  }




  Future<void> updateItem(int usuario_id, String usuario_nombre, String usuario_email, String usuario_password) async {
    final response = await http.put(Uri.parse('$serverUrl/users/$usuario_id'),
        headers: {
          'Content-Type': 'application/json',
        },
        body: jsonEncode({'usuario_id': usuario_id,'usuario_nombre': usuario_nombre, 'usuario_email': usuario_email, 'usuario_password': usuario_password}));

    if (response.statusCode != 200) {
      throw Exception("Error actualizando el usuario");
    }
  }


  Future<void> updateItemproducto(int producto_id, String producto_nombre, double producto_calorias, double producto_proteinas, double producto_grasas, int usuario_id) async {
    final response = await http.put(Uri.parse('$serverUrl/api/products/$producto_id'),
        headers: {
          'Content-Type': 'application/json',
        },
        body: jsonEncode({'producto_id': producto_id,'producto_nombre': producto_nombre, 'producto_calorias': producto_calorias, 'producto_proteinas': producto_proteinas, 'producto_grasas': producto_grasas, 'usuario_id': usuario_id}));

    if (response.statusCode != 200) {
      throw Exception("Error actualizando el producto");
    }
  }

  Future<void> updateItemalimentacion(int alimentacion_id,String alimentacion_fecha, int producto_id, double alimentacion_cantidad, int usuario_id) async {
    final response = await http.put(Uri.parse('$serverUrl/api/food/ali/$alimentacion_id'),
        headers: {
          'Content-Type': 'application/json',
        },
        body: jsonEncode({'alimentacion_id': alimentacion_id,'alimentacion_fecha': alimentacion_fecha, 'producto_id': producto_id, 'alimentacion_cantidad': alimentacion_cantidad, 'usuario_id': usuario_id}));

    if (response.statusCode != 200) {
      throw Exception("Error actualizando la alimentacion");
    }
  }


  Future<void> deleteItem(int usuario_id) async {
    final response =
    await http.delete(Uri.parse('$serverUrl/users/$usuario_id'));

    if (response.statusCode != 200) {
      throw Exception("Error eliminando el usuario");
    }
  }

  Future<void> deleteItemproduct(int producto_id) async {
    final response =
    await http.delete(Uri.parse('$serverUrl/api/products/$producto_id'));

    if (response.statusCode != 200) {
      throw Exception("Error eliminando el producto");
    }
  }

  Future<void> deleteItemproductfood(int alimentacion_id) async {
    final response =
    await http.delete(Uri.parse('$serverUrl/api/food/ali/$alimentacion_id'));

    if (response.statusCode != 200) {
      throw Exception("Error eliminando la alimentacion");
    }
  }


  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Column(
          children: [













          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          showDialog(
              context: context,
              builder: (context) {
                return AlertDialog(
                  title: const Text('Agregar usuario'),
                  content:SingleChildScrollView(
                    child:Container(
                      child:Column(
                        children: [
                          TextFormField(
                            controller: nameController,
                            decoration: const InputDecoration(
                              labelText: 'Nombre',
                            ),
                          ),
                          TextFormField(
                            controller: emailController,
                            decoration: const InputDecoration(
                              labelText: 'Email',
                            ),
                          ),
                          TextFormField(
                            controller: passwordController,
                            decoration: const InputDecoration(
                              labelText: 'Password',
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),


                  actions: [
                    TextButton(
                      onPressed: () {
                        Navigator.pop(context);
                      },
                      child: const Text('Cancelar'),
                    ),
                    TextButton(
                      onPressed: () {
                        addItem(
                            nameController.text, emailController.text, passwordController.text);
                        setState(() {
                          nameController.clear();
                          emailController.clear();
                          passwordController.clear();
                        });
                        Navigator.pop(context);
                      },
                      child: const Text('Guardar'),
                    ),
                  ],
                );
              });
        },
        tooltip: 'Agregar usuario',
        child: const Icon(Icons.add),
      ),


    );
  }
}