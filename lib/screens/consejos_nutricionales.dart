import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:login_register/screens/macronutrientes.dart';
import 'package:login_register/screens/preguntas_frecuentes.dart';
//import 'package:login_register/screens/page5.dart';
import 'package:login_register/widgets/drawer_menu.dart';

import 'home_page.dart';


class ConsejosNutricionales extends StatelessWidget{
  final int elusuarioid;
  const ConsejosNutricionales(this.elusuarioid,{Key? key}) : super(key:key);
  @override
  Widget build(BuildContext context){
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.lightBlueAccent, // Color de fondo de la barra de la aplicación
        title: Text('App de calorias'), // Título de la barra de la aplicación
      ),
      // bottomNavigationBar: NavigationBar(
      //   backgroundColor: Colors.lightBlueAccent,// Barra de navegación en la parte inferior
      //   indicatorColor: Colors.amber,  // Índice de la pestaña seleccionada
      //   destinations: const <Widget>[ // Lista de destinos para las pestañas
      //     NavigationDestination(
      //       selectedIcon: Icon(Icons.home), // Icono seleccionado para la pestaña "Home"
      //       icon: Icon(Icons.home_outlined), // Icono para la pestaña "Home"
      //       label: 'Productos', // Etiqueta de la pestaña "Home"
      //     ),
      //     NavigationDestination(
      //       icon: Badge(child: Icon(Icons.account_balance_wallet_sharp)), // Icono para la pestaña "Notifications" con un badge
      //       label: 'Conteo', // Etiqueta de la pestaña "Notifications"
      //     ),
      //     NavigationDestination(
      //       icon: Badge( // Icono para la pestaña "Messages" con un badge
      //         label: Text('1'),
      //         child: Icon(Icons.messenger_sharp),
      //       ),
      //       label: 'Perfil', // Etiqueta de la pestaña "Messages"
      //     ),
      //   ],
      // ),
      drawer: DrawerMenu(this.elusuarioid),
      body: Container(
        margin: EdgeInsets.only(left: 20, top: 20, right: 20),
        child:Column(
          children: [
            Text('Bienvenido usuario: '+elusuarioid.toString()),
            Text('Consejos nutriciones: ', style: TextStyle(color: Colors.lightBlue, fontSize: 15),),
            SizedBox(height: 20,),
            Expanded(child: Container(
              child:Column(
                children: [
                  Container(
                    decoration: BoxDecoration(color: Colors.yellow, borderRadius: BorderRadius.circular(30.0)),
                    child: Row(
                      children: [
                        Container(
                          height: 100.0,
                          width: 80.0,
                          margin: EdgeInsets.all(8.0),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(30.0),
                            image: DecorationImage(
                              image: NetworkImage("https://www.paho.org/sites/default/files/alimentacao-saudavel-diversificada.jpg"),
                              fit: BoxFit.cover,
                            ),
                          ),
                        ),
                        Column(
                          children: [
                            Text('Consejo para una alimentación\n balanceada', style: TextStyle(fontWeight: FontWeight.bold),),
                            Text('Disminuya el consumo de\n grasas saturadas\n y evite las comidas rápidas.'),
                          ],
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: 10,),
                  Container(
                    decoration: BoxDecoration(color: Colors.orangeAccent, borderRadius: BorderRadius.circular(30.0)),
                    child: Row(
                      children: [
                        Container(
                          height: 100.0,
                          width: 80.0,
                          margin: EdgeInsets.all(8.0),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(30.0),
                            image: DecorationImage(
                              image: NetworkImage("https://mejorconsalud.as.com/wp-content/uploads/2013/12/alimentos-mas-nutritivos.jpg"),
                              fit: BoxFit.cover,
                            ),
                          ),
                        ),
                        Column(
                          children: [
                            Text('Poque consumir una\n amplia gama alimenticia?', style: TextStyle(fontWeight: FontWeight.bold),),
                            Text('Cada tipo de alimento tiene\n diferentes nutrientes, vitaminas\n que son\n importantes para el cuerpo.'),
                          ],
                        ),

                      ],
                    ),
                  ),

                  SizedBox(height: 10,),
                  Container(
                    decoration: BoxDecoration(color: Colors.green, borderRadius: BorderRadius.circular(30.0)),
                    child: Row(
                      children: [
                        Container(
                          height: 100.0,
                          width: 80.0,
                          margin: EdgeInsets.all(8.0),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(30.0),
                            image: DecorationImage(
                              image: NetworkImage("https://ichef.bbci.co.uk/ace/ws/640/cpsprodpb/89E2/production/_106589253_amino.jpg"),
                              fit: BoxFit.cover,
                            ),
                          ),
                        ),
                        Column(
                          children: [
                            Text('Como mejorar la\n absorcion de nutrientes?', style: TextStyle(fontWeight: FontWeight.bold),),
                            Text('Cuando incluyas carnes\n rojas o vísceras, puedes\n combinarlas con limón,\n naranja o mandarina.'),
                          ],
                        ),

                      ],
                    ),
                  ),
                  SizedBox(height: 10,),
                  Expanded(child: Container(

                    child: SizedBox(
                      height: 150.0,
                      child: ListView(
                          children: [
                            CarouselSlider(
                              items: [
                                Container(
                                  height: 150.0,
                                  margin: EdgeInsets.all(8.0),
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(30.0),
                                    image: DecorationImage(
                                      image: NetworkImage("https://i.masquemedicos.org/services/big-esmeraldas-nutricionista-kelly-penafiel-jimenez-20210703140733jagb.jpg"),
                                      fit: BoxFit.cover,
                                    ),
                                  ),
                                ),
                                Container(
                                  height: 150.0,
                                  margin: EdgeInsets.all(8.0),
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(30.0),
                                    image: DecorationImage(
                                      image: NetworkImage("https://cdn.eup.eus/medias/NUTRICIONISTA-1200Wx1200H?context=bWFzdGVyfGltYWdlc3wzMTg3NTN8aW1hZ2UvanBlZ3xpbWFnZXMvaDcwL2hmOS85Mjg4NTgyNDk2Mjg2LmpwZ3wxODEyMGExOWQ4ZjNkNjUxOGM2ZWNmYzJmODRkZTE2N2NjOWE5NmJiYzk0NTdhOWVhM2Q1MmUzZmM0OGRmMWE5"),
                                      fit: BoxFit.cover,
                                    ),
                                  ),
                                ),
                                Container(
                                  height: 150.0,
                                  margin: EdgeInsets.all(8.0),
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(30.0),
                                    image: DecorationImage(
                                      image: NetworkImage("https://d2lcsjo4hzzyvz.cloudfront.net/blog/wp-content/uploads/2021/09/07142912/nutricionista-doctoraki.jpg"),
                                      fit: BoxFit.cover,
                                    ),
                                  ),
                                ),
                              ],
                              options: CarouselOptions(
                                height: 150.0,
                                enlargeCenterPage: true,
                                autoPlay: true,
                                aspectRatio: 16 / 9,
                                autoPlayCurve: Curves.fastOutSlowIn,
                                enableInfiniteScroll: true,
                                autoPlayAnimationDuration: Duration(milliseconds: 800),
                                viewportFraction: 0.8,
                              ),
                            ),
                          ]
                      ),
                    ),
                  )),
                  SizedBox(height: 10,),
                ],
              ),
            ),
            ),
          ],
        ),
      ),
    );
  }
}