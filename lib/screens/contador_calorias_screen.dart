import 'dart:math';

import 'package:flutter/material.dart';
import 'package:login_register/mocks/mock_data.dart';
import 'package:login_register/models/alimentacion.dart';

class ContadorDeCalorias extends StatefulWidget {
  const ContadorDeCalorias({super.key});

  @override
  State<ContadorDeCalorias> createState() => _ContadorDeCaloriasState();
}

class _ContadorDeCaloriasState extends State<ContadorDeCalorias> {
  late List<Alimentacion> _alimentos = [];
  @override
  void initState() {
    super.initState();
    // Llama a la función deseada aquí
    _fetchAlimentacion(); // Llama a una función que ejecuta la obtención de datos
  }

  Future<void> _fetchAlimentacion() async {
    try {
      List<Alimentacion> alimentaciones =
          await MockData.getAlimentacion(int.parse(MockData.user!.id));
      setState(() {
        _alimentos = alimentaciones;
      });
    } catch (error) {
      print('Error al obtener las alimentaciones: $error');
      // Maneja el error aquí si es necesario
    }
  }

  @override
  Widget build(BuildContext context) {
    return ListView(
      children: [
        Container(
          padding: EdgeInsets.all(15),
          margin: EdgeInsets.symmetric(vertical: 10),
          decoration: BoxDecoration(
            // color: Colors.orange.withOpacity(0.1),
            borderRadius: BorderRadius.circular(10),
          ),
          alignment: Alignment.center,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                "Calorías Totales",
                style: TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.bold,
                  color: Colors.blue,
                ),
              ),
              SizedBox(height: 5),
              Text(
                "${_calcularCaloriasTotales()}",
                style: TextStyle(
                  fontSize: 24,
                  fontWeight: FontWeight.bold,
                  color: Colors.blueAccent,
                ),
              ),
            ],
          ),
        ),
        Container(
          padding: EdgeInsets.all(15),
          margin: EdgeInsets.symmetric(vertical: 10),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(10),
            // boxShadow: [
            //   BoxShadow(
            //     color: Colors.grey.withOpacity(0.5),
            //     spreadRadius: 1,
            //     blurRadius: 3,
            //     offset: Offset(0, 2), // changes position of shadow
            //   ),
            // ],
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              _buildNutrientContainer(
                title: "Grasas",
                value: "${_calcularGrasasTotales()}",
                color: Colors.green,
              ),
              _buildNutrientContainer(
                title: "Carbohidratos",
                value: "${_calcularCarbohiddratosTotales()}",
                color: Colors.red,
              ),
              _buildNutrientContainer(
                title: "Proteínas",
                value: "${_calcularProteinasTotales()}",
                color: Colors.purple,
              ),
            ],
          ),
        ),
        Container(
          padding: EdgeInsets.all(10),
          margin: EdgeInsets.symmetric(vertical: 10),
          decoration: BoxDecoration(
            color: Colors.grey.withOpacity(0.1),
            borderRadius: BorderRadius.circular(10),
          ),
          child: Column(
            children: [
              Text(
                "Alimentos Consumidos",
                style: TextStyle(
                  fontSize: 18,
                  fontWeight: FontWeight.bold,
                  color: Colors.blue,
                ),
              ),
              Container(
                height: 350,
                child: _listAlimentos(),
              ),
            ],
          ),
        ),
      ],
    );
  }

  Widget _buildNutrientContainer({
    required String title,
    required String value,
    required Color color,
  }) {
    return Container(
      padding: EdgeInsets.symmetric(
        vertical: 10,
        horizontal: 5,
      ),
      decoration: BoxDecoration(
        color: color.withOpacity(0.1),
        borderRadius: BorderRadius.circular(10),
      ),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            value,
            style: TextStyle(
              fontSize: 16,
              fontWeight: FontWeight.bold,
              color: color,
            ),
          ),
          SizedBox(height: 5),
          Text(
            title,
            style: TextStyle(
              fontSize: 14,
              color: color,
            ),
          ),
        ],
      ),
    );
  }

  double _calcularCaloriasTotales() {
    print(_alimentos);
    if (_alimentos == null || _alimentos.isEmpty) {
      return 0;
    }

    double totalCalorias = 0;
    for (var alimentacion in _alimentos) {
      if (alimentacion.producto != null) {
        print(alimentacion);
        totalCalorias += alimentacion.cantidad * alimentacion.producto.calorias;
      }
    }
    return totalCalorias;
  }

  double _calcularGrasasTotales() {
    if (_alimentos == null || _alimentos.isEmpty) {
      return 0;
    }

    double totalGrasas = 0;
    for (var alimentacion in _alimentos) {
      if (alimentacion.producto != null) {
        totalGrasas += alimentacion.cantidad * alimentacion.producto.grasas;
      }
    }
    return totalGrasas;
  }

  double _calcularCarbohiddratosTotales() {
    if (_alimentos == null || _alimentos.isEmpty) {
      return 0;
    }

    double totalCarbohidratos = 0;
    for (var alimentacion in _alimentos) {
      if (alimentacion.producto != null) {
        totalCarbohidratos +=
            alimentacion.cantidad * alimentacion.producto.carbohidratos;
      }
    }
    return totalCarbohidratos;
  }

  double _calcularProteinasTotales() {
    if (_alimentos == null || _alimentos.isEmpty) {
      return 0;
    }

    double totalProteinas = 0;
    for (var alimentacion in _alimentos) {
      if (alimentacion.producto != null) {
        totalProteinas +=
            alimentacion.cantidad * alimentacion.producto.proteinas;
      }
    }
    return totalProteinas;
  }

  ListView _listAlimentos() {
    return ListView.builder(
      shrinkWrap: true,
      itemCount: _alimentos.length,
      itemBuilder: (BuildContext context, int index) {
        final alimento = _alimentos[index];
        return Card(
          margin: EdgeInsets.symmetric(vertical: 5, horizontal: 10),
          child: ListTile(
            title: Text(alimento.producto.nombre),
            subtitle: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text('Calorías: ${alimento.producto.calorias}'),
                Text('Cantidad: ${alimento.cantidad}'),
              ],
            ),
            trailing: IconButton(
              icon: Icon(Icons.delete),
              onPressed: () {
                setState(() {
                  MockData.deleteAlimentacion(alimento.id!);
                  _alimentos.remove(alimento);
                });
              },
            ),
          ),
        );
      },
    );
  }

}
