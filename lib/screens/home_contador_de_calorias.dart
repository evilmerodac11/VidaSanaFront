import 'package:flutter/material.dart';

import 'package:login_register/mocks/mock_data.dart';
import 'package:login_register/models/usuario.dart';
import 'package:login_register/screens/contador_calorias_screen.dart';
import 'package:login_register/screens/producto_form_screen.dart';
import 'package:login_register/screens/producto_list_screen.dart';
import 'package:login_register/widgets/drawer_menu.dart';

class HomeContadorCalorias extends StatefulWidget {
  final int elusuarioid;
  const HomeContadorCalorias(this.elusuarioid,{super.key});

  @override
  State<HomeContadorCalorias> createState() => _HomeContadorCaloriasState();
}

class _HomeContadorCaloriasState extends State<HomeContadorCalorias> {
  int currentPageIndex = 0;
  Usuario user=MockData.user!;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.pink,
        title: Text('Contador de calorias'),
      ),
      drawer:DrawerMenu(widget.elusuarioid),
      body: Container(
        child: [
          ProductScreen(user),
          ProductListScreen(user),
          ContadorDeCalorias()][currentPageIndex],
      ),
      bottomNavigationBar: NavigationBar(
        onDestinationSelected: (int index) {
          setState(() {
            currentPageIndex = index;
          });
        },
        indicatorColor: Colors.pink,
        selectedIndex: currentPageIndex,
        destinations: const <Widget>[
          NavigationDestination(
            selectedIcon: Icon(Icons.add,color: Colors.white,),
            icon: Icon(Icons.add_circle_outline),
            label: 'Agregar',
          ),
          NavigationDestination(
            selectedIcon: Icon(Icons.list,color: Colors.white,),

            icon:  Icon(Icons.list),
            label: 'Listar',
          ),
          NavigationDestination(
            selectedIcon: Icon(Icons.summarize,color: Colors.white,),

            icon: Icon(Icons.summarize),
            label: 'Sumario',
          ),
        ],
      ),
    );
  }
}
