import 'package:flutter/material.dart';
import 'package:login_register/screens/home_contador_de_calorias.dart';
import 'package:login_register/screens/home_page.dart';
import 'package:login_register/screens/consejos_nutricionales.dart';
import 'package:login_register/screens/macronutrientes.dart';
import 'package:login_register/screens/preguntas_frecuentes.dart';
//import 'package:login_register/screens/page5.dart';

import '../screens/login_page.dart';

class DrawerMenu extends StatelessWidget {
  final int elusuarioid;
  const DrawerMenu(this.elusuarioid,{super.key});

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: [
          const DrawerHeader( // Encabezado del drawer
            decoration: BoxDecoration(
              color: Colors.blue,
            ),
            child: Text('Opciones de la App'), // Texto del encabezado del drawer
          ),
          ListTile(
            title: const Text('Informacion general'), // O// Marca si está seleccionado
            onTap: () {
              Route route=MaterialPageRoute(builder: (_)=>PageStatefull(this.elusuarioid));
              Navigator.pushReplacement(context, route);
            },
          ),
          ListTile(
            title: const Text('Consejos nutricionales'), // Marca si está seleccionado
            onTap: () {
              Route route=MaterialPageRoute(builder: (_)=>ConsejosNutricionales(this.elusuarioid));
              Navigator.pushReplacement(context, route);
            },
          ),
          ListTile(
            title: const Text('Los macronutrientes'), /// Marca si está seleccionado
            onTap: () {
              Route route=MaterialPageRoute(builder: (_)=>Macronutrientes(this.elusuarioid));
              Navigator.pushReplacement(context, route);
            },
          ),

          ListTile(
            title: const Text('Preguntas frecuentes'), /// Marca si está seleccionado
            onTap: () {
              Route route=MaterialPageRoute(builder: (_)=>PreguntasFrecuentes(this.elusuarioid));
              Navigator.pushReplacement(context, route);
            },
          ),
          ListTile(
            title: const Text('Ingreso de alimentos'), /// Marca si está seleccionado
            onTap: () {
              Route route=MaterialPageRoute(builder: (_)=>HomeContadorCalorias( this.elusuarioid));
              Navigator.pushReplacement(context, route);
            },
          ),
          ListTile(
            leading: Image.network(('https://cdn-icons-png.freepik.com/512/152/152534.png')),
            onTap: () {
              Route route=MaterialPageRoute(builder: (_)=>LoginPage());
              Navigator.pushReplacement(context, route);
            },
          ),
        ],
      ),
    );
  }
}
